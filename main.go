package main

import (
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

func main() {
	logger := logrus.New()
	cfg, err := parseArgs(logger)
	if err != nil {
		os.Exit(1)
	}

	// modify resolv.conf
	filePath := "/etc/resolv.conf"
	input, err := os.ReadFile(filePath)
	if err != nil {
		logger.WithError(err).Fatal("failed to read resolv.conf")
	}

	isNameserverSet := false
	var baseNameserver string
	resolvConfLine := "nameserver ::1"
	emptyResolvConfLine := "nameserver 0.0.0.0"
	lines := strings.Split(string(input), "\n")
	for i, line := range lines {
		if strings.Contains(line, resolvConfLine) && strings.Contains(lines[i+1], emptyResolvConfLine) && strings.Contains(lines[i+2], emptyResolvConfLine) {
			isNameserverSet = true
		}
		if strings.Contains(line, "### BASE NAMESERVER ###") {
			baseNameserver = strings.TrimPrefix(lines[i+1], "nameserver ")
		}
	}

	if !isNameserverSet {
		// add avahi2dns as nameserver to resolv.conf
		// 2 empty lines to force avahi2dns as only server
		lines = append([]string{resolvConfLine, emptyResolvConfLine, emptyResolvConfLine}, lines...)
	}

	output := strings.Join(lines, "\n")
	err = os.WriteFile(filePath, []byte(output), 0644)
	if err != nil {
		logger.WithError(err).Fatal("failed to write resolv.conf")
	}

	// run dns server
	err = runServer(logger, cfg, baseNameserver)
	if err != nil {
		logger.WithError(err).Fatal("server failed to start")
	}
}
