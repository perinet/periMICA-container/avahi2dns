module github.com/LouisBrunner/avahi2dns

go 1.18

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/godbus/dbus/v5 v5.1.0
	github.com/holoplot/go-avahi v1.0.1
	github.com/miekg/dns v1.1.51
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/alexflint/go-scalar v1.1.0 // indirect
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/tools v0.3.0 // indirect
)
