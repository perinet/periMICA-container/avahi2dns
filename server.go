package main

import (
	"fmt"

	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"
	"github.com/miekg/dns"
	"github.com/sirupsen/logrus"
)

func runServer(logger *logrus.Logger, cfg *config, dnsNameserver string) error {
	// connect to dbus
	logger.Debug("connection to dbus...")
	conn, err := dbus.SystemBus()
	if err != nil {
		return fmt.Errorf("connection to dbus failed: %w", err)
	}

	// connect to avahi server
	logger.Debug("connection to avahi through dbus...")
	aserver, err := avahi.ServerNew(conn)
	if err != nil {
		return fmt.Errorf("connection to avahi failed: %w", err)
	}

	// add mdns handlers
	mdnsHandler := func(w dns.ResponseWriter, r *dns.Msg) {
		logger.Debug("mdnsHandler")
		rlogger := logger.WithField("component", "main")
		rlogger.WithField("request", r).Debug("received request")
		m := createDNSReply(rlogger, aserver, r, cfg.IPv6)
		rlogger.WithField("reply", m).Debug("sending reply")
		m.SetReply(r)
		w.WriteMsg(m)
	}

	for _, domain := range cfg.Domains {
		logger.WithField("domain", domain).Debug("adding mdns handler")
		dns.HandleFunc(fmt.Sprintf("%s.", domain), mdnsHandler)
	}

	// add dns handler
	dnsHandler := func(w dns.ResponseWriter, r *dns.Msg) {
		logger.Debug("dnsHandler")
		rlogger := logger.WithField("component", "main")
		rlogger.WithField("request", r).Debug("received request")
		c := new(dns.Client)
		m, _, err := c.Exchange(r, "["+dnsNameserver+"]:53")
		if err != nil {
			rlogger.WithField("err", err).Error("reply error")
			return
		}
		m.Compress = true
		rlogger.WithField("reply", m).Debug("sending reply")
		w.WriteMsg(m)
	}

	logger.WithField("domain", ".").Debug("adding dns handler")
	dns.HandleFunc(".", dnsHandler)

	// start DNS server
	dserver := &dns.Server{
		Addr: fmt.Sprintf("%s:%d", cfg.BindAddr, cfg.Port),
		Net:  "udp",
	}
	logger.WithField("addr", dserver.Addr).Info("starting DNS server")
	err = dserver.ListenAndServe()
	defer dserver.Shutdown()
	if err != nil {
		return fmt.Errorf("failed to start DNS server: %w", err)
	}
	return nil
}
